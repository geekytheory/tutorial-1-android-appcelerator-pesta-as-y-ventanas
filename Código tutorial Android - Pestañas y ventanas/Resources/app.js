/*
 * 
 *Tutorial 1 programación Android con Appcelerator.
 * Autor: Geeky Theory
 * http://geekytheory.com
 * 
 */


//Creo un grupo de pestañas con createTabGroup()
var tabGroup = Titanium.UI.createTabGroup();

//Añado una ventana, que estará unida a la primera pestaña
var ventana1 = Titanium.UI.createWindow({
	Title : 'Ventana 1',
	backgroundColor : 'black'
});

//Añado una pestaña y le asigno la ventana1 anteriormente creada
var tab1 = Titanium.UI.createTab({
	window : ventana1,
	icon : 'KS_nav_ui.png',
	title: 'Tab 1'
});


//Creo una nueva ventana para la segunda pestaña
var ventana2 = Ti.UI.createWindow({
	Title : 'Ventana 2',
	backgroundColor : 'black'
});

//Creo una nueva pestaña pestaña i le asigno la ventana2 que acabo de crear
var tab2 = Ti.UI.createTab({
	window : ventana2,
	icon : 'KS_nav_views.png',
	title: 'Tab 2'
});


//Añado a cada ventana un texto identificativo que nos diga en qué ventana estamos

var texto_ventana1 = Ti.UI.createLabel({
			text : 'Soy la ventana 1',
			font : {
				fontSize : 24,
				fontFamily : 'Helvetica'
			},
			top : '30'
		});
		ventana1.add(texto_ventana1);

var texto_ventana2 = Ti.UI.createLabel({
			text : 'Soy la ventana 2',
			font : {
				fontSize : 24,
				fontFamily : 'Helvetica'
			},
			top : '30'
		});
		ventana2.add(texto_ventana2);

//Añado las pestañas tab1 y tab2 al grupo de pestañas
tabGroup.addTab(tab1);
tabGroup.addTab(tab2);

//Abro el grupo de pestañas
tabGroup.open();

